# linuxsoft-get-puppet-ips

This repository runs the [linuxsoft-get-puppet-ips](https://gitlab.cern.ch/linuxsupport/rpms/linuxsoft-rhel-access) script periodically

# This script is deprecated!
More info: https://its.cern.ch/jira/browse/LOS-1260
