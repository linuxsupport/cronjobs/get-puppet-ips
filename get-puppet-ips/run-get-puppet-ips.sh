#!/bin/bash

TOP="/puppet-ips"
UPDINFOPY="/root/get_puppet_ips.py"
JSONFILE="linuxsoft-get-puppet-ips.json"

echo $LINUXCI_PWD | kinit $LINUXCI_USER

echo "[landb]" > /root/credentials.ini
echo "landbuser = $LINUXCI_USER" >> /root/credentials.ini
echo "landbpass = $LINUXCI_PWD" >> /root/credentials.ini

echo "$UPDINFOPY --path $TOP --jsonfile $JSONFILE"
$UPDINFOPY --path $TOP --jsonfile $JSONFILE
/usr/bin/mv -f $TOP/$JSONFILE.new $TOP/$JSONFILE

echo "done."
