#!/usr/bin/python3
"""Get IPs of all puppet nodes our PuppetDB knows about and printout a flat json"""
import argparse
import json
import sys
import requests
from requests_kerberos import HTTPKerberosAuth

def parse_args():
    """ define command line arguments"""
    aparser = argparse.ArgumentParser(
        description='Get logs for a distribution repo access')
    aparser.add_argument('--path',
                        help='Directory path to store the file',
                        action='store',
                        dest='dir_path')
    aparser.add_argument('--jsonfile',
                    help='Json filename',
                    action='store',
                    dest='json_file')
    return aparser.parse_args()


def get_puppetdb_ips():
    """Get IP v4/v6 address from PuppetDB"""
    urls = ['https://constable.cern.ch:9081/pdb/query/v4/facts/ipaddress',
            'https://constable.cern.ch:9081/pdb/query/v4/facts/ipaddress6']
    auth = HTTPKerberosAuth()
    args = parse_args()
    puppetdb_ips = {}
    for url in urls:
        pdb_data = requests.get(url, auth=auth)
        for ipadd in pdb_data.json():
            puppetdb_ips[ipadd['value']] = "true"

    json_object = json.dumps(puppetdb_ips, indent=4)
    with open(f"{args.dir_path}/{args.json_file}.new", "w", encoding='utf-8') as outfile:
        outfile.write(json_object)

if __name__ == '__main__':
    sys.exit(get_puppetdb_ips())
